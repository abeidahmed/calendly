# frozen_string_literal: true

class MarketingConstraint
  def self.matches?(request)
    request.script_name.blank?
  end
end
