class OrganizationConstraint
  def self.matches?(request)
    organization_id = request.script_name&.delete("/")
    return false if organization_id.blank?

    Organization.exists?(id: organization_id)
  end
end
