class OrganizationUrlPrefixer
  def initialize(app)
    @app = app
  end

  def call(env)
    request = ActionDispatch::Request.new(env)
    _, organization_id, request_path = request.path.split("/", 3)

    if /\d+/.match?(organization_id)
      if (organization = Organization.find_by(id: organization_id))
        Current.organization = organization
      else
        return [302, {"Location" => "/not_found"}, []]
      end

      request.script_name = "/#{organization_id}"
      request.path_info = "/#{request_path}"
    end

    @app.call(request.env)
  end
end
