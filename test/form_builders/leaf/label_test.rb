# frozen_string_literal: true

require "test_helper"

class Leaf::LabelTest < ActiveSupport::TestCase
  def test_renders_label
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.label :email
      end
    end

    assert_selector "label[for='user_email']", text: "Email"
  end

  def test_label_text
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email, "Email address"
      end
    end

    assert_selector "label", text: "Email address"
  end

  def test_renders_block
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email do
          "Your email address"
        end
      end
    end

    assert_selector "label", text: "Your email address"
  end

  def test_class
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email, class: "foo-label"
      end
    end

    assert_selector "label.font-semibold.foo-label"
  end
end
