# frozen_string_literal: true

require "test_helper"

class Leaf::SubmitTest < ActiveSupport::TestCase
  def test_renders_submit_button
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit
      end
    end

    assert_selector "button[type='submit']", text: "Create"
  end

  def test_button_text
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit "Sign up"
      end
    end

    assert_selector "button[type='submit']", text: "Sign up"
  end

  def test_custom_class
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit class: "custom-class"
      end
    end

    assert_selector ".bg-blue-500.custom-class", text: "Create"
  end
end
