# frozen_string_literal: true

require "test_helper"

class Leaf::CheckBoxTest < ActiveSupport::TestCase
  def test_render_check_box
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.check_box :subscribe
      end
    end

    assert_selector "input[type='checkbox']"
    assert_selector "input[type='checkbox'][name='user[subscribe]']"
  end
end
