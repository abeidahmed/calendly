# frozen_string_literal: true

require "test_helper"

class Leaf::PasswordFieldTest < ActiveSupport::TestCase
  def test_renders_text_field
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.password_field :password
      end
    end

    assert_selector "input[type='password']"
    assert_selector "input[name='user[password]']"
  end
end
