require "test_helper"

class OrganizationUrlPrefixerTest < ActiveSupport::TestCase
  test "loads the organization" do
    organization = create(:organization)
    organization_path = "/#{organization.id}"
    path = "/posts"
    full_path = "http://example.com#{organization_path}#{path}"

    app = ->(env) { [200, env] }
    middleware = OrganizationUrlPrefixer.new(app)
    response = Rack::MockRequest.new(middleware).get(full_path, "REQUEST_PATH" => full_path)

    assert_equal organization, Current.organization
    assert_equal organization_path, response["SCRIPT_NAME"]
    assert_equal path, response["PATH_INFO"]
  end

  test "redirects to not_found page if organization id is invalid" do
    full_path = "http://example.com/1234/posts"

    app = ->(env) { [200, env] }
    middleware = OrganizationUrlPrefixer.new(app)
    response = Rack::MockRequest.new(middleware).get(full_path, "REQUEST_PATH" => full_path)

    assert_equal "/not_found", response.headers["Location"]
    assert_nil Current.organization
    assert_nil response["SCRIPT_NAME"]
    assert_nil response["PATH_INFO"]
  end

  test "does not interfere if organization_id is not present" do
    path = "/posts"
    full_path = "http://example.com#{path}"

    app = ->(env) { [200, env] }
    middleware = OrganizationUrlPrefixer.new(app)
    response = Rack::MockRequest.new(middleware).get(full_path, "REQUEST_PATH" => full_path)

    assert_equal path, response["PATH_INFO"]
    assert_nil Current.organization
    assert response["SCRIPT_NAME"].blank?
  end
end
