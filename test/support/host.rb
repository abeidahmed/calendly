# frozen_string_literal: true

module Support
  module Host
    def set_organization(organization = nil)
      if organization
        host! "example.com/#{organization.id}"
      else
        host "example.com"
      end
    end
  end
end
