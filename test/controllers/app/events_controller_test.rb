require "test_helper"

class App::EventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = create(:organization_with_members)
    @member = @organization.members.last

    sign_in(@member.user)
    set_organization(@organization)
  end

  test "creating an event with valid params" do
    freeze_time

    assert_difference "@member.events.count", 1 do
      post events_path, params: {
        event: {
          title: "Lunch with Mike",
          start_datetime: Time.current,
          end_datetime: Time.current + 20.minutes,
          time_zone: "Kolkata",
          recurring_rule: nil
        }
      }
    end

    event = Event.last
    assert_equal "Lunch with Mike", event.title
    assert_equal Time.current, event.start_datetime
    assert_equal Time.current + 20.minutes, event.end_datetime
    assert_equal "Kolkata", event.time_zone
    assert_not event.all_day?
    assert event.recurring_rule.is_a?(Hash)
    assert_equal @organization, event.organization
    assert_equal @member, event.creator

    assert_response :created

    unfreeze_time
  end

  test "creating an event with invalid params" do
    assert_no_difference "Event.count" do
      post events_path, params: {event: {title: "", start_datetime: Time.current, end_datetime: ""}}
    end

    assert_equal %i[title end_datetime], json_error
    assert_response :unprocessable_entity
  end
end
