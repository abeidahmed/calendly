FactoryBot.define do
  factory :user do
    first_name { "Jack" }
    last_name { "Man" }
    sequence(:email) { |n| "jack#{n}@example.com" }
    password { "secret_password" }
  end
end
