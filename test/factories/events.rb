FactoryBot.define do
  factory :event do
    organization
    association :creator, factory: :member
    sequence(:title) { |n| "Event title #{n}" }
    start_datetime { Time.current - 2.hours }
    end_datetime { Time.current }
    recurring_rule { IceCube::Rule.daily.count(1) }
    time_zone { "UTC" }
  end
end
