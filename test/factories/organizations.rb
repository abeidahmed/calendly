FactoryBot.define do
  factory :organization do
    sequence(:name) { |n| "Organization #{n}" }
    association :owner, factory: :user

    factory :organization_with_members do
      after(:create) do |organization|
        organization.members.create!(user: organization.owner)
      end
    end
  end
end
