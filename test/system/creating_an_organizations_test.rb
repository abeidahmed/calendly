require "application_system_test_case"

class CreatingAnOrganizationsTest < ApplicationSystemTestCase
  test "redirects to dashboard path after creating an organization" do
    user = create(:user)
    sign_in(user)
    visit new_organization_path

    assert_text "Create an organization"

    fill_in "organization_name", with: "My organization"
    click_on "Create"
    assert_selector "[data-test-id='organization-switcher']"
  end
end
