require "application_system_test_case"

class SignUpsTest < ApplicationSystemTestCase
  test "redirects to organization new page" do
    visit new_user_registration_path
    fill_in "user[name]", with: "Jannie Doe"
    fill_in "user[email]", with: "jannie@example.com"
    fill_in "user[password]", with: "secret_password"
    click_button "Sign up"

    assert_current_path new_organization_path
  end
end
