require "test_helper"

class EventTest < ActiveSupport::TestCase
  subject { build(:event) }

  context "associations" do
    should belong_to(:organization)
    should belong_to(:creator).class_name("Member")
  end

  context "validations" do
    should validate_presence_of(:title)
    should validate_length_of(:title).is_at_most(255)
    should validate_presence_of(:start_datetime)
    should validate_presence_of(:end_datetime)
    should validate_presence_of(:time_zone)
    should validate_inclusion_of(:time_zone).in_array(ActiveSupport::TimeZone.all.map(&:name))
  end

  test "start_datetime is less than end_datetime" do
    event = build(:event, start_datetime: Time.current + 2.hours, end_datetime: Time.current)
    assert_not event.valid?
    assert event.errors[:start_datetime].include?("should be less than end datetime")
  end

  test "start_datetime cannot be equal to end_datetime" do
    freeze_time

    event = build(:event, start_datetime: Time.current, end_datetime: Time.current)
    assert_not event.valid?
    assert event.errors[:start_datetime].include?("should be less than end datetime")

    unfreeze_time
  end

  test "returns recurring_rule as a Hash" do
    event = create(:event)
    assert_equal RecurringRule.dirty_hash_to_rule(event.recurring_rule).to_hash, event.recurring_rule
  end

  test "defaults to non-recurring rule if recurring_rule is empty" do
    event = build(:event, recurring_rule: nil)
    assert_equal IceCube::Rule.daily.count(1), IceCube::Rule.from_hash(event.recurring_rule)
  end

  test "sets the span_datetime to the schedules' end_datetime" do
    event = create(:event, recurring_rule: IceCube::Rule.daily.count(4))
    assert_equal event.schedule.all_occurrences[-1], event.span_datetime
  end

  test "sets the span_datetime to nil if schedule is infinitely recurring" do
    event = create(:event, recurring_rule: IceCube::Rule.daily)
    assert event.span_datetime.nil?
  end

  test "updates the span_datetime accordingly" do
    event = create(:event, recurring_rule: IceCube::Rule.daily)
    assert event.span_datetime.nil?

    event.update(recurring_rule: IceCube::Rule.daily.count(4))
    event.reload

    assert_equal event.schedule.all_occurrences[-1], event.span_datetime
  end

  test ".between" do
    travel_to Time.zone.parse("2022-11-7")

    event = create(:event, start_datetime: Time.current, end_datetime: Time.current + 20.minutes, recurring_rule: nil)
    event2 = create(:event, start_datetime: Time.current - 1.day, end_datetime: (Time.current - 1.day) + 20.minutes, recurring_rule: IceCube::Rule.daily.until(Time.current + 1.day))
    event3 = create(:event, start_datetime: Time.zone.parse("2022-10-1"), end_datetime: Time.zone.parse("2022-10-1") + 20.minutes, recurring_rule: IceCube::Rule.weekly)
    create(:event, start_datetime: Time.zone.parse("2022-10-1"), end_datetime: Time.zone.parse("2022-10-1") + 5.minutes, recurring_rule: nil)
    create(:event, start_datetime: Time.zone.parse("2023-01-1"), end_datetime: Time.zone.parse("2023-01-1") + 10.minutes)

    assert_equal [event.id, event2.id, event3.id], Event.between(Time.current, Time.current + 5.days).ids

    travel_back
  end

  test "instantiatiating start_datetime also updates end_datetime" do
    freeze_time

    event = create(:event, start_datetime: Time.current, end_datetime: Time.current + 1200)
    new_event = event.instantiate(start_datetime: Time.current + 1.day)
    assert_equal (Time.current + 1.day + 1200), new_event.end_datetime

    unfreeze_time
  end

  test ".occurrences_between" do
    create(:event, start_datetime: Time.zone.parse("2022-11-1"), end_datetime: Time.zone.parse("2022-11-1") + 20.minutes, recurring_rule: IceCube::Rule.daily.count(10)) # 3 occurrences
    create(:event, start_datetime: Time.zone.parse("2022-10-26"), end_datetime: Time.zone.parse("2022-10-26") + 20.minutes, recurring_rule: IceCube::Rule.daily.count(7)) # 2 occurrences
    occurrences = Event.occurrences_between(Time.zone.parse("2022-10-31"), Time.zone.parse("2022-11-3"))

    assert_equal 5, occurrences.length
  end

  test "#occurrences_between" do
    event = create(:event, start_datetime: Time.zone.parse("2022-11-1"), end_datetime: Time.zone.parse("2022-11-1") + 20.minutes, recurring_rule: IceCube::Rule.daily.count(10))
    occurrences = event.occurrences_between(Time.zone.parse("2022-11-5"), Time.zone.parse("2022-11-7"))

    assert_equal 3, occurrences.length
  end
end
