# frozen_string_literal: true

require "test_helper"

class IconButtonComponentTest < ViewComponent::TestCase
  test "renders icon button" do
    render_preview(:default)

    assert_selector "button[type='button']"
  end

  test "title" do
    render_inline(IconButtonComponent.new(icon: :user, title: "Custom title"))

    assert_selector "button[title='Custom title']"
  end

  test "size" do
    render_inline(IconButtonComponent.new(icon: :user, title: "Custom title", size: 40))

    assert_selector "button" do
      assert_selector "svg[height='40'][width='40']"
    end
  end

  test "renders anchor" do
    render_inline(IconButtonComponent.new(tag: :a, href: "#", icon: :user, title: "Custom title"))

    assert_selector "a[href='#']"
    refute_selector "[type='button']"
  end

  test "disabled button" do
    render_inline(IconButtonComponent.new(icon: :user, disabled: true, title: "Custom title"))

    assert_selector "button[disabled]"
    assert_selector "button[aria-disabled='true']"
    assert_selector "button[tabindex='-1']"
  end
end
