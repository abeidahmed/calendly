# frozen_string_literal: true

require "test_helper"

module Helpers
  class FetchOrFallbackHelperTest < ActiveSupport::TestCase
    include FetchOrFallbackHelper

    def test_fetches_from_hash
      variant_mappings = {
        primary: {
          default: "default-value",
          disabled: "disabled-value"
        }
      }.freeze

      value = fetch_or_fallback(variant_mappings, :primary, :primary, :disabled)
      assert_equal "disabled-value", value
    end

    def test_fallback_hash
      variant_mappings = {
        primary: {
          default: "default-value",
          disabled: "disabled-value"
        }
      }.freeze

      value = fetch_or_fallback(variant_mappings, :secondary, :primary, :default)
      assert_equal "default-value", value
    end

    def test_fallback_nested_hash_when_key_missing
      variant_mappings = {
        primary: {
          default: "default-value",
          disabled: "disabled-value"
        }
      }.freeze

      value = fetch_or_fallback(variant_mappings, :secondary, :primary)
      assert_equal "default-value", value
    end

    def test_fetches_from_array
      value = fetch_or_fallback([:primary, :secondary], :primary, :primary)

      assert_equal :primary, value
    end

    def test_fallback_array
      value = fetch_or_fallback([:primary, :secondary], :invalid, :primary)

      assert_equal :primary, value
    end

    def test_fetches_from_hash_keys
      hash = {primary: "blue", secondary: "yellow"}
      value = hash[fetch_or_fallback(hash.keys, :secondary, :primary)]

      assert_equal "yellow", value
    end

    def test_fallback_hash_keys
      hash = {primary: "blue", secondary: "yellow"}
      value = hash[fetch_or_fallback(hash.keys, :invalid, :primary)]

      assert_equal "blue", value
    end
  end
end
