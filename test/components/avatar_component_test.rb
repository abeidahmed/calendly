# frozen_string_literal: true

require "test_helper"

class AvatarComponentTest < ViewComponent::TestCase
  test "renders avatar" do
    render_preview(:default)

    assert_selector "img[height='32'][width='32']"
  end

  test "src" do
    render_preview(:default, params: {src: "https://url.com"})

    assert_selector "img[src='https://url.com']"
  end

  test "alt text" do
    render_preview(:default, params: {alt: "Custom alt text"})

    assert_selector "img[alt='Custom alt text']"
  end

  test "size" do
    render_preview(:default, params: {size: 28})

    assert_selector "img[height='28'][width='28']"
  end

  test "custom class" do
    render_inline(AvatarComponent.new(src: "", alt: "", class: "custom-class"))

    assert_selector "img.rounded-full.custom-class"
  end
end
