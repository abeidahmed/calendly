# frozen_string_literal: true

require "test_helper"

class ButtonComponentTest < ViewComponent::TestCase
  def test_renders_button
    render_inline(ButtonComponent.new) { "Button" }

    assert_selector "button[type='button']", text: "Button"
  end

  def test_renders_anchor
    render_inline(ButtonComponent.new(tag: :a, href: "#")) { "Button" }

    assert_selector "a[href='#']", text: "Button"
    refute_selector "[type='button']"
  end

  def test_disabled_button
    render_inline(ButtonComponent.new(disabled: true)) { "Button" }

    assert_selector "button[disabled]"
    assert_selector "button[aria-disabled='true']"
    assert_selector "button[tabindex='-1']"
  end
end
