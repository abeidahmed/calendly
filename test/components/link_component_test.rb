# frozen_string_literal: true

require "test_helper"

class LinkComponentTest < ViewComponent::TestCase
  def test_renders_link
    render_inline(LinkComponent.new) { "Sign in" }

    assert_selector "a[href='#']", text: "Sign in"
    assert_selector ".text-blue-600"
    refute_selector "a[type]"
  end

  def test_renders_button
    render_inline(LinkComponent.new(tag: :button)) { "Sign in" }

    assert_selector "button[type='button']", text: "Sign in"
    assert_selector ".text-blue-600"
    refute_selector "button[href]"
  end

  def test_button_type
    render_inline(LinkComponent.new(tag: :button, type: :submit)) { "Sign in" }

    assert_selector "button[type='submit']", text: "Sign in"
  end

  def test_custom_class
    render_inline(LinkComponent.new(class: "custom-class")) { "Sign in" }

    assert_selector ".text-blue-600.custom-class", text: "Sign in"
  end
end
