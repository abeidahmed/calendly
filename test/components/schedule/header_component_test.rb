# frozen_string_literal: true

require "test_helper"

class Schedule::HeaderComponentTest < ViewComponent::TestCase
  test "render today link" do
    with_request_url "/" do
      render_inline(Schedule::HeaderComponent.new(start_date: Date.current, end_date: Date.current + 5.days))

      assert_selector "a[href='#{schedule_months_path}']", count: 1
    end
  end

  test "renders next/prev view link" do
    with_request_url "/" do
      freeze_time
      start_date = Date.current
      end_date = Date.current + 5.days
      render_inline(Schedule::HeaderComponent.new(start_date: start_date, end_date: end_date))

      assert_selector "a[href='/?start_date=#{(start_date - 1.day).iso8601}']"
      assert_selector "a[href='/?start_date=#{(end_date + 1.day).iso8601}']"
      unfreeze_time
    end
  end

  test "renders new event button" do
    with_request_url "/" do
      render_inline(Schedule::HeaderComponent.new(start_date: Date.current, end_date: Date.current + 5.days))

      assert_selector "button[type='button']", text: "New event"
    end
  end
end
