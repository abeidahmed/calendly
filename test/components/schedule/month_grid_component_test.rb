# frozen_string_literal: true

require "test_helper"

# http://localhost:3000/1/schedule/month?start_date=2022-11-15
class Schedule::MonthGridComponentTest < ViewComponent::TestCase
  test "today renders the abbreviated month name" do
    with_request_url "/" do
      travel_to Time.zone.parse("2022-11-15")
      render_inline(Schedule::MonthGridComponent.new(start_date: Date.current))

      day = Date.current.beginning_of_month
      assert_selector "h2[data-test-id='#{day}-name']", text: day.strftime("%d %b") # 01 Nov
      travel_back
    end
  end

  test "same month day does not render the month name" do
    with_request_url "/" do
      travel_to Time.zone.parse("2022-11-15")
      render_inline(Schedule::MonthGridComponent.new(start_date: Date.current))

      day = Date.current
      assert_selector "h2[data-test-id='#{day}-name']", text: day.strftime("%d") # 15
      travel_back
    end
  end

  test "start date of previous month renders the abbreviated month name" do
    with_request_url "/" do
      travel_to Time.zone.parse("2022-11-15")
      render_inline(Schedule::MonthGridComponent.new(start_date: Date.current))

      day = Date.current.beginning_of_month.beginning_of_week
      assert_selector "h2[data-test-id='#{day}-name']", text: day.strftime("%d %b") # 30 Oct
      travel_back
    end
  end

  test "start date of next month renders the abbreviated month name" do
    with_request_url "/" do
      travel_to Time.zone.parse("2022-11-15")
      render_inline(Schedule::MonthGridComponent.new(start_date: Date.current))

      day = Date.current.next_month.beginning_of_month
      assert_selector "h2[data-test-id='#{day}-name']", text: day.strftime("%d %b") # 01 Dec
      travel_back
    end
  end
end
