# frozen_string_literal: true

require "test_helper"

class BannerComponentTest < ViewComponent::TestCase
  test "renders banner" do
    render_preview(:default)

    assert_selector "div[role='alert']" do
      assert_selector "button[title='Dismiss']"
    end
  end

  test "renders with action button" do
    render_preview(:with_action_button)

    assert_selector "div[role='alert']" do
      assert_selector "[data-test-id='action-btn']"
    end
  end

  test "render with action content" do
    render_preview(:with_action_content)

    assert_selector "div[role='alert']" do
      assert_selector "[data-test-id='action-content-btn']"
    end
  end

  test "banner attributes when dismissible" do
    render_preview(:default)

    assert_data "[role='alert']", attribute: "data-controller", value: "banner"
    assert_data "button[title='Dismiss']", attribute: "data-action", value: "click->banner#hide"
  end

  test "render without dismiss button" do
    render_preview(:default, params: {dismissible: false})

    refute_selector "[role='alert'][data-controller='banner']"
    refute_selector "button[title='Dismiss']"
  end
end
