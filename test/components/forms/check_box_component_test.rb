# frozen_string_literal: true

require "test_helper"

class Forms::CheckBoxComponentTest < ViewComponent::TestCase
  def test_renders
    render_inline(Forms::CheckBoxComponent.new(:user, :subscribe))

    assert_selector "input[type='checkbox'][value='1']"
    assert_selector "input[type='checkbox'][id='user_subscribe']"
    assert_selector "input[type='checkbox'][name='user[subscribe]']"
    assert_selector "input[type='checkbox'][autocomplete='off']"
  end

  def test_renders_hidden_field
    render_inline(Forms::CheckBoxComponent.new(:user, :subscribe))

    assert_selector "input[type='hidden']", visible: false
    assert_selector "input[name='user[subscribe]']", visible: false
    assert_selector "input[autocomplete='off']", visible: false
  end
end
