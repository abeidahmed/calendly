# frozen_string_literal: true

require "test_helper"

class Forms::HelpTextComponentTest < ViewComponent::TestCase
  def test_renders_help_text
    render_inline(Forms::HelpTextComponent.new) { "Help text" }

    assert_selector "[data-test-id='form-help-text']", text: "Help text"
  end

  def test_custom_class
    render_inline(Forms::HelpTextComponent.new(class: "custom-help-class")) { "Help text" }

    assert_selector ".text-gray-600.custom-help-class"
  end
end
