# frozen_string_literal: true

require "test_helper"

class Forms::GroupComponentTest < ViewComponent::TestCase
  def test_renders
    render_inline(Forms::GroupComponent.new) { "Content" }

    assert_selector ".form-group"
  end

  def test_does_not_render_without_content
    render_inline(Forms::GroupComponent.new)

    refute_component_rendered
  end

  def test_custom_class
    render_inline(Forms::GroupComponent.new(class: "custom-class")) { "Content" }

    assert_selector ".form-group.custom-class"
  end
end
