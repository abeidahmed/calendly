# frozen_string_literal: true

require "test_helper"

class Forms::LabelComponentTest < ViewComponent::TestCase
  def test_renders_label
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address"))

    assert_selector "label", text: "Email address"
    assert_selector "label[for='user_email']"
  end

  def test_renders_block
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address")) { "Email" }

    assert_selector "label", text: "Email"
  end

  def test_custom_class
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address", class: "custom-class"))

    assert_selector ".font-semibold.custom-class"
  end
end
