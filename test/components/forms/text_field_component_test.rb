# frozen_string_literal: true

require "test_helper"

class Forms::TextFieldComponentTest < ViewComponent::TestCase
  test "renders text field" do
    render_inline(Forms::TextFieldComponent.new(:user, :name))

    assert_selector "input[type='text']"
    assert_selector "input[name='user[name]']"
    assert_selector "input[id='user_name']"
    refute_selector "input[aria-describedby]"
    refute_selector "[data-test-id='user']"
  end

  test "custom class" do
    render_inline(Forms::TextFieldComponent.new(:user, :name, class: "custom-class"))

    assert_selector ".block.w-full.custom-class"
  end

  test "help text" do
    render_inline(Forms::TextFieldComponent.new(:user, :name, help_text: "My help text"))

    assert_selector "[data-test-id='form-help-text']", text: "My help text"

    help_text = page.find("[data-test-id='form-help-text']")
    assert_selector "input[aria-describedby='#{help_text[:id]}']"
  end

  test "prefix icon" do
    render_inline(Forms::TextFieldComponent.new(:user, :name)) do |c|
      c.with_prefix_icon(icon: "user")
    end

    assert_selector "input[type='text'].pl-10"
    assert_selector "[data-test-id='prefix-icon']"
  end
end
