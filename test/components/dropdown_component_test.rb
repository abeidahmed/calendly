# frozen_string_literal: true

require "test_helper"

class DropdownComponentTest < ViewComponent::TestCase
  test "does not render without button" do
    render_inline(DropdownComponent.new) { |c| c.with_menu { "Menu" } }

    refute_component_rendered
  end

  test "does not render without menu" do
    render_inline(DropdownComponent.new) { |c| c.with_button { "Button" } }

    refute_component_rendered
  end

  test "renders dropdown" do
    render_preview(:default)

    assert_selector "div[data-test-id='dropdown']" do
      assert_selector "button[data-test-id='dropdown-btn']", text: "Toggle dropdown"
      assert_selector "div[data-test-id='dropdown-menu']", visible: false do
        assert_selector "a[data-test-id='dropdown-item']", count: 3, visible: false
        assert_selector "hr[data-test-id='dropdown-divider']", count: 1, visible: false
      end
    end
  end

  test "container attributes" do
    render_preview(:default)

    assert_data "[data-test-id='dropdown']", attribute: "data-controller", value: "dropdown"
    assert_data "[data-test-id='dropdown']", attribute: "data-action", value: "keydown->dropdown#onKeydown focusin@window->dropdown#onOutsideFocus"
    assert_data "[data-test-id='dropdown']", attribute: "data-dropdown-placement-value", value: "bottom-start"
  end

  test "button attributes" do
    render_preview(:default)

    assert_data "[data-test-id='dropdown-btn']", attribute: "data-action", value: "click->dropdown#toggle"
    assert_data "[data-test-id='dropdown-btn']", attribute: "data-dropdown-target", value: "btn"
  end

  test "menu attributes" do
    render_preview(:default)

    assert_data "[data-test-id='dropdown-menu']", attribute: "data-dropdown-target", value: "menu", visible: false
  end

  test "item attributes" do
    render_inline(DropdownComponent.new) do |c|
      c.with_button { "Button" }
      c.with_menu do |m|
        m.with_item { "Edit" }
      end
    end

    assert_data "[data-test-id='dropdown-item']", attribute: "data-action", value: "click->dropdown#selectItem", visible: false
    assert_selector "[data-test-id='dropdown-item'][tabindex='0']", visible: false

    refute_selector "[data-test-id='dropdown-item'][disabled]", visible: false
    refute_selector "[data-test-id='dropdown-item'][aria-disabled='true']", visible: false
  end

  test "disabled item" do
    render_inline(DropdownComponent.new) do |c|
      c.with_button { "Button" }
      c.with_menu do |m|
        m.with_item(disabled: true) { "Edit" }
      end
    end

    assert_data "[data-test-id='dropdown-item']", attribute: "data-action", value: "click->dropdown#selectItem", visible: false
    assert_selector "[data-test-id='dropdown-item'][tabindex='-1']", visible: false
    assert_selector "[data-test-id='dropdown-item'][disabled]", visible: false
    assert_selector "[data-test-id='dropdown-item'][aria-disabled='true']", visible: false
  end

  test "divider item" do
    render_inline(DropdownComponent.new) do |c|
      c.with_button { "Button" }
      c.with_menu do |m|
        m.with_item(divider: true) { "Item" }
      end
    end

    assert_selector "[data-test-id='dropdown-divider'][role='separator']", visible: false
    assert_selector "[data-test-id='dropdown-divider'][tabindex='-1']", visible: false
    assert_selector "hr[data-test-id='dropdown-divider']", visible: false

    refute_selector "[data-test-id='dropdown-divider'][data-action]", visible: false
    refute_selector "[data-test-id='dropdown-divider'][data-dropdown-target]", visible: false
    refute_selector "[data-test-id='dropdown-divider']", text: "Item", visible: false # does not render content
  end

  test "anchor item" do
    render_inline(DropdownComponent.new) do |c|
      c.with_button { "Button" }
      c.with_menu do |m|
        m.with_item(href: "#") { "Item" }
      end
    end

    assert_selector "[data-test-id='dropdown-item'][href='#']", text: "Item", visible: false
    refute_selector "[data-test-id='dropdown-item'][type='button']", visible: false
  end

  test "button item" do
    render_inline(DropdownComponent.new) do |c|
      c.with_button { "Button" }
      c.with_menu do |m|
        m.with_item(tag: :button) { "Item" }
      end
    end

    assert_selector "[data-test-id='dropdown-item'][type='button']", text: "Item", visible: false
    refute_selector "[data-test-id='dropdown-item'][href]", visible: false
  end
end
