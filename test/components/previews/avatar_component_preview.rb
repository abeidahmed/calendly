# frozen_string_literal: true

class AvatarComponentPreview < ViewComponent::Preview
  # @param src text
  # @param alt text
  # @param size number
  def default(src: "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80", alt: "Avatar", size: 32)
    render(AvatarComponent.new(src: src, alt: alt, size: size))
  end
end
