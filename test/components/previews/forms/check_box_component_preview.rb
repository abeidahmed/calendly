# frozen_string_literal: true

class Forms::CheckBoxComponentPreview < ViewComponent::Preview
  def default
    render(Forms::CheckBoxComponent.new(:user, :subscribe))
  end
end
