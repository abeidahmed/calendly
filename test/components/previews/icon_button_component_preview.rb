# frozen_string_literal: true

class IconButtonComponentPreview < ViewComponent::Preview
  # @param icon select [user, bell]
  # @param title text
  # @param variant select [default, primary, success, warning, danger]
  # @param icon_variant select [outline, solid, mini]
  # @param disabled toggle
  def default(icon: "user", title: "User menu", variant: :default, icon_variant: :outline, disabled: false)
    render(IconButtonComponent.new(icon: icon, title: title, variant: variant, icon_variant: icon_variant, disabled: disabled))
  end
end
