# frozen_string_literal: true

class ButtonComponentPreview < ViewComponent::Preview
  # @param variant select [default, primary, danger]
  # @param disabled toggle
  def default(variant: ButtonComponent::DEFAULT_VARIANT, disabled: false)
    render(ButtonComponent.new(variant: variant, disabled: disabled)) { "Button" }
  end
end
