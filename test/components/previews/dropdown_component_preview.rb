# frozen_string_literal: true

class DropdownComponentPreview < ViewComponent::Preview
  def default
    render(DropdownComponent.new) do |c|
      c.with_button { "Toggle dropdown" }
      c.with_menu do |m|
        m.with_item(href: "#") { "Edit" }
        m.with_item(href: "#") { "Transfer..." }
        m.with_item(divider: true)
        m.with_item(href: "#") { "Delete" }
      end
    end
  end
end
