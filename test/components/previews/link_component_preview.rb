# frozen_string_literal: true

class LinkComponentPreview < ViewComponent::Preview
  def default
    render(LinkComponent.new) { "Sign in" }
  end
end
