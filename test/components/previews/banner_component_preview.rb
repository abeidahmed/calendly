# frozen_string_literal: true

class BannerComponentPreview < ViewComponent::Preview
  # @param variant select [notice, success, alert, warning]
  # @param dismissible toggle
  # @param full toggle
  def default(variant: BannerComponent::DEFAULT_VARIANT, dismissible: true, full: false)
    render(BannerComponent.new(variant: variant, dismissible: dismissible, full: full)) { "Big news! We're excited to announce a brand new product." }
  end

  # @param variant select [notice, success, alert, warning]
  # @param dismissible toggle
  # @param full toggle
  def with_action_button(variant: BannerComponent::DEFAULT_VARIANT, dismissible: true, full: false)
    render_with_template(locals: {variant: variant, dismissible: dismissible, full: full})
  end

  # @param variant select [notice, success, alert, warning]
  # @param dismissible toggle
  # @param full toggle
  def with_action_content(variant: BannerComponent::DEFAULT_VARIANT, dismissible: true, full: false)
    render_with_template(locals: {variant: variant, dismissible: dismissible, full: full})
  end
end
