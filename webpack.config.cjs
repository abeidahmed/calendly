const { ESBuildMinifyPlugin } = require('esbuild-loader');
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');

module.exports = {
  mode: process.env.NODE_ENV,
  entry: {
    application: './app/javascript/application.ts',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './app/assets/builds'),
  },
  resolve: {
    extensions: ['.js', '.ts', '.vue'],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.ts$/,
        loader: 'esbuild-loader',
        options: {
          loader: 'ts',
          tsconfigRaw: require('./tsconfig.json'),
        },
      },
    ],
  },
  optimization: {
    minimizer: [new ESBuildMinifyPlugin({ target: 'esnext' })],
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({ __VUE_OPTIONS_API__: false, __VUE_PROD_DEVTOOLS__: false }),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
  ],
};
