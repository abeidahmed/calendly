/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/views/**/*.haml',
    './app/javascript/**/*.{js,ts,vue}',
    './app/components/**/*.{haml,rb}',
    './app/helpers/**/*.rb',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Helvetica',
          'Arial',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji',
        ],
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
