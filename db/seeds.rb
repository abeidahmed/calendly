user = User.find_by(email: "mamakane@gmail.com")
organization = user.owned_organizations.last
return unless organization

member = Member.find_by(user_id: user.id, organization_id: organization.id)

Event.create!(
  title: "Single event",
  organization: organization,
  start_datetime: Time.current,
  end_datetime: Time.current + 3600,
  time_zone: "Kolkata",
  all_day: false,
  creator: member,
  recurring_rule: IceCube::Rule.daily.count(1)
)

Event.create!(
  title: "Recurring event",
  organization: organization,
  start_datetime: Time.current - 2.days,
  end_datetime: (Time.current - 2.days) + 3600,
  time_zone: "Kolkata",
  all_day: false,
  creator: member,
  recurring_rule: IceCube::Rule.daily.count(3)
)
