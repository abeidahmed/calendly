class AddRecurringRuleToEvent < ActiveRecord::Migration[7.0]
  def change
    add_column :events, :recurring_rule, :jsonb, null: false
  end
end
