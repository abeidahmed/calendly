class AddSpanDatetimeToEvents < ActiveRecord::Migration[7.0]
  def change
    add_column :events, :span_datetime, :datetime
  end
end
