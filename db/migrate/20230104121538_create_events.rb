class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.belongs_to :organization, null: false, foreign_key: true
      t.string :title, null: false
      t.datetime :start_datetime, null: false
      t.datetime :end_datetime, null: false
      t.string :time_zone, null: false, default: "UTC"
      t.boolean :all_day, null: false, default: false
      t.integer :creator_id, null: false

      t.timestamps
    end

    add_index :events, :creator_id
  end
end
