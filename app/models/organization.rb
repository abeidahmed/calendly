class Organization < ApplicationRecord
  belongs_to :owner, class_name: "User", foreign_key: :owner_id
  has_many :members, dependent: :destroy
  has_many :events, dependent: :destroy

  validates :name, presence: true, length: {maximum: 255}
end
