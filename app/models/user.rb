class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  has_person_name

  has_many :owned_organizations, class_name: "Organization", foreign_key: :owner_id, inverse_of: :owner, dependent: :destroy
  has_many :memberships, class_name: "Member", dependent: :destroy

  validates :first_name, presence: true, length: {maximum: 127}
  validates :last_name, length: {maximum: 127}
  validates :time_zone, presence: true, inclusion: {in: ActiveSupport::TimeZone.all.map(&:name)}
end
