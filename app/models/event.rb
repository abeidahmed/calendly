class Event < ApplicationRecord
  acts_as_tenant :organization
  belongs_to :creator, class_name: "Member"

  before_save :set_span_datetime

  validates :title, presence: true, length: {maximum: 255}
  validates :start_datetime, presence: true
  validates :end_datetime, presence: true
  validates :time_zone, presence: true, inclusion: {in: ActiveSupport::TimeZone.all.map(&:name)}
  validate :start_datetime_less_than_end_datetime

  # Finds all the events between a given `start_datetime` and `end_datetime`
  # For terminating events
  # - we want to get all the events that ends between the `start_datetime` and `end_datetime`
  # - we want to get all the events that starts between the `start_datetime` and `end_datetime`
  # - we want to get all the events where the `start_datetime` is between the event's start_datetime and end_datetime
  # For non-terminating events
  # - we want to get all the events whose start_datetime is less than the `end_datetime`
  scope :between, ->(start_datetime, end_datetime) {
    where(
      "(span_datetime BETWEEN :start_datetime AND :end_datetime) OR
      (:start_datetime BETWEEN start_datetime AND span_datetime) OR
      (start_datetime BETWEEN :start_datetime AND :end_datetime) OR
      (start_datetime <= :end_datetime AND span_datetime IS NULL)",
      start_datetime: start_datetime, end_datetime: end_datetime
    )
  }

  def recurring_rule
    RecurringRule.dirty_hash_to_rule(super)&.to_hash
  end

  def recurring_rule=(value)
    rule = if RecurringRule.valid_rule?(value)
      RecurringRule.dirty_hash_to_rule(value).to_hash
    else
      # Non-recurring event
      IceCube::Rule.daily.count(1)
    end

    super(rule)
  end

  # Initializes the schedule and adds the event's recurring rule
  # Note: The result is memoized intentionally, so be careful when using in the same response cycle
  def schedule
    @schedule ||= IceCube::Schedule.new(start_datetime, end_time: end_datetime)
    @schedule.tap do |s|
      s.add_recurrence_rule(IceCube::Rule.from_hash(recurring_rule))
    end
  end

  def self.occurrences_between(start_datetime, end_datetime)
    events = between(start_datetime, end_datetime)
    events.flat_map { |event| event.occurrences_between(start_datetime, end_datetime) }
  end

  def occurrences_between(start_datetime, end_datetime)
    schedule.occurrences_between(start_datetime, end_datetime).map { |date| instantiate(start_datetime: date) }
  end

  # In recurring events, ids are not unique because we clone the same event. Hence, we create a unique id with the
  # event's `start_datetime` and `id`
  def uid
    "#{id}-#{start_datetime.to_i}"
  end

  def instantiate(options = {})
    start_time = options.delete(:start_datetime)
    # Update end_datetime so that duration of the event remains consistent
    end_time = start_time + (end_datetime - start_datetime)

    self.class.new(
      id: id,
      title: title,
      start_datetime: start_time,
      end_datetime: end_time,
      span_datetime: span_datetime,
      all_day: all_day,
      time_zone: time_zone,
      recurring_rule: recurring_rule,
      organization: organization,
      creator: creator,
      created_at: created_at,
      updated_at: updated_at,
      **options
    )
  end

  private

  # Span datetime is the total duration of the event + it's last recurrence end time
  # Eg:
  # If an event is recurring, then it's span time would be event.start_datetime + event.last_recurrence.end_datetime
  # If an event is recurring infinitely, it's span_time is nil
  def set_span_datetime
    # We cannot use the memoized `schedule` method here
    ice_cube_schedule = IceCube::Schedule.new(start_datetime, end_time: end_datetime)
    ice_cube_schedule.add_recurrence_rule(IceCube::Rule.from_hash(recurring_rule))
    self.span_datetime = ice_cube_schedule.last
  rescue ArgumentError
    self.span_datetime = nil
  end

  def start_datetime_less_than_end_datetime
    return unless start_datetime && end_datetime

    if start_datetime >= end_datetime
      errors.add(:start_datetime, :less_than_end_datetime)
    end
  end
end
