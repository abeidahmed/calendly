class Member < ApplicationRecord
  acts_as_tenant :organization
  belongs_to :user
  has_many :events, foreign_key: :creator_id, inverse_of: :creator, dependent: :destroy

  validates :user, uniqueness: {scope: :organization_id, message: "is already in the organization"}
end
