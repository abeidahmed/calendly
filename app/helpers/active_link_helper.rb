require "addressable"

module ActiveLinkHelper
  # Usage
  # = active_link_to "View profile", users_path, class: "menu", active_class: "menu-active", inactive_class: "menu-inactive", data: { ... }
  #
  # = active_link_to users_path, class: "menu", active_class: "menu-active", inactive_class: "menu-inactive", data: { ... } do
  #   %span Edit profile
  def active_link_to(*args, &block)
    name = block ? capture(&block) : args.shift
    options = args.shift || {}
    html_options = args.shift || {}
    url = url_for(options)

    active_options = {}
    link_options = {}
    html_options.each do |k, v|
      if %i[active_class inactive_class match_type].include?(k)
        active_options[k] = v
      else
        link_options[k] = v
      end
    end

    css_class = link_options.delete(:class).to_s + " "
    if active_link?(url, match_type: active_options[:match_type])
      css_class << (active_options[:active_class].to_s.presence || "")
      link_options[:"aria-current"] = :page
    else
      css_class << (active_options[:inactive_class].to_s.presence || "")
    end

    css_class.strip!
    link_options[:class] = css_class if css_class.present?
    link_to(name, url, link_options)
  end

  # Usage
  # active_link?(users_path)
  # active_link?(users_path, match_type: :exclusive)
  # active_link?(users_path, match_type: :exact)
  # active_link?(users_path, match_type: /users\/\d/)
  # active_link?(users_path, match_type: true)
  # active_link?(users_path, match_type: false)
  def active_link?(url, match_type: nil)
    parsed_url = Addressable::URI.parse(url).path
    path = request.original_fullpath

    case match_type
    when :exclusive
      path.match(/^#{Regexp.escape(parsed_url)}\/?(\?.*)?$/).present?
    when :exact
      path == url
    when Regexp
      path.match(match_type).present?
    when TrueClass
      true
    when FalseClass
      false
    else
      path.match(/^#{Regexp.escape(parsed_url).chomp('/')}(\/.*|\?.*)?$/).present?
    end
  end
end
