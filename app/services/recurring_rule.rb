class RecurringRule
  def self.dirty_hash_to_rule(params)
    return params if params.is_a?(IceCube::Rule)

    params = JSON.parse(params, quirks_mode: true) if params.is_a?(String)
    return if params.nil?

    params = params.symbolize_keys
    rules_hash = filter_params(params)
    IceCube::Rule.from_hash(rules_hash)
  end

  def self.valid_rule?(possible_rule)
    return true if possible_rule.is_a?(IceCube::Rule)
    return false if possible_rule.blank?

    if possible_rule.is_a?(String)
      begin
        return JSON.parse(possible_rule).is_a?(Hash)
      rescue JSON::ParserError
        return false
      end
    end

    return true if possible_rule.is_a?(Hash)
    false
  end

  private

  class << self
    def filter_params(params)
      params.reject! { |_key, value| value.blank? || value == "null" }

      params[:interval] = params[:interval].to_i if params[:interval]
      params[:week_start] = params[:week_start].to_i if params[:week_start]

      params[:validations] ||= {}
      params[:validations].symbolize_keys!
      params[:validations][:day] = params[:validations][:day].collect(&:to_i) if params[:validations][:day]
      params
    end
  end
end
