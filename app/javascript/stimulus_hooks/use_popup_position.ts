import { autoUpdate, computePosition, flip, offset, Placement, shift, Strategy } from '@floating-ui/dom';
import type { Controller } from '@hotwired/stimulus';
import { nextTick } from '../utils/timing';

type Options = {
  reference: HTMLElement;
  popup: HTMLElement;
  placement?: Placement;
  strategy?: Strategy;
  distance?: number;
  skidding?: number;
};

export type UsePopupPositionType = {
  start: () => void;
  update: () => Promise<void>;
  stop: () => void;
};

export default function usePopupPosition(
  controller: Controller,
  { reference, popup, placement = 'bottom-start', strategy = 'absolute', distance = 4, skidding = 0 }: Options
) {
  let cleanup: ReturnType<typeof autoUpdate>;

  async function handlePositioning() {
    const middleware = [offset({ mainAxis: distance, crossAxis: skidding }), flip(), shift()];
    await nextTick();
    const { x, y } = await computePosition(reference, popup, { placement, strategy, middleware });
    popup.setAttribute('data-placement', placement);

    Object.assign(popup.style, {
      left: `${x}px`,
      top: `${y}px`,
      position: strategy,
    });
  }

  function start() {
    cleanup = autoUpdate(reference, popup, handlePositioning);
    handlePositioning();
  }

  async function update() {
    await handlePositioning();
  }

  function stop() {
    cleanup?.();
  }

  // Make a clone of `disconnect` function
  const controllerDisconnect = () => controller.disconnect.bind(controller);

  Object.assign(controller, {
    disconnect() {
      stop();
      controllerDisconnect();
    },
  });

  return { start, update, stop } as const;
}
