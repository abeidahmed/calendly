/**
 * Inspired from https://github.com/tailwindlabs/headlessui/blob/main/packages/%40headlessui-react/src/hooks/use-outside-click.ts
 */
import type { Controller } from '@hotwired/stimulus';

type OutsideClickController = {
  isOpen: boolean;
  onOutsideClick?: (event: MouseEvent | PointerEvent, target: HTMLElement) => void;
} & Controller;
type Boundary = HTMLElement | null | undefined;

type OutsideClickOptions = {
  boundaries: Boundary | Boundary[];
  callback?: (event: MouseEvent | PointerEvent, target: HTMLElement) => void;
};

export default function useOutsideClick(
  controller: OutsideClickController,
  { boundaries, callback }: OutsideClickOptions
) {
  let initialClickTarget: EventTarget | undefined;

  function onMousedown(event: MouseEvent) {
    initialClickTarget = event.composedPath?.()?.[0] || event.target;
  }

  function onMouseclick(event: MouseEvent) {
    onOutsideClick(event, () => {
      return initialClickTarget as HTMLElement;
    });

    initialClickTarget = undefined;
  }

  function onOutsideClick<E extends MouseEvent | PointerEvent>(
    event: E,
    resolveTarget: (event: E) => HTMLElement | null
  ) {
    if (!controller.isOpen || event.defaultPrevented) return;

    const _boundaries = (function resolve(boundaries) {
      if (Array.isArray(boundaries)) {
        return boundaries;
      }

      return [boundaries];
    })(boundaries);

    const target = resolveTarget(event);
    if (!target) return;
    if (!target.getRootNode().contains(target)) return;

    for (const boundary of _boundaries) {
      if (!boundary) continue;
      if (boundary.contains(target)) return;
      if (event.composed && event.composedPath().includes(boundary)) return;
    }

    const cb = controller?.onOutsideClick || callback;
    cb?.(event, target);
  }

  function observe() {
    document.addEventListener('mousedown', onMousedown, true);
    document.addEventListener('click', onMouseclick, true);
  }

  function unobserve() {
    document.removeEventListener('mousedown', onMousedown, true);
    document.removeEventListener('click', onMouseclick, true);
  }

  // Make a clone of `disconnect` function
  const controllerDisconnect = () => controller.disconnect.bind(controller);

  Object.assign(controller, {
    disconnect() {
      unobserve();
      controllerDisconnect();
    },
  });

  observe();

  return { observe, unobserve } as const;
}
