import { readonly } from 'vue';
import { createVueApp } from '../utils/vue';
import WeekRow from './components/week_row.vue';

createVueApp(WeekRow, '.js-week-row', (app, element) => {
  const { startDate, endDate } = element.dataset;
  if (!startDate || !endDate) return;

  app.provide('startDate', readonly<Date>(new Date(startDate)));
  app.provide('endDate', readonly<Date>(new Date(endDate)));
});
