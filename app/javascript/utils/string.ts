/**
 * @description Returns a unique id with `sketch` as the prefix
 */
export function brandedId(infix = ''): string {
  return ['sketch', infix, Math.random().toString().slice(2, 6)].filter(Boolean).join('-');
}
