export type MoveDirection = 1 | -1;

/**
 * @description Cycles through an array of elements and returns the next/previous element
 */
export function move<T>(elements: T[], activeElement: T, index: MoveDirection): T | undefined {
  if (elements.length === 0) return undefined;
  let activeIndex = elements.indexOf(activeElement);

  if (activeIndex === elements.length - 1 && index === 1) {
    activeIndex = -1;
  }

  let indexOfElement = index === 1 ? 0 : elements.length - 1;
  if (activeElement && activeIndex >= 0) {
    const newIndex = activeIndex + index;
    if (newIndex >= 0 && newIndex < elements.length) {
      indexOfElement = newIndex;
    }
  }

  return elements[indexOfElement];
}

/**
 * @description Splits an array into a defined size with the format [['foo', 'bar'], ['baz', 'bat']]
 */
export function chunks<T>(array: T[], size: number): T[][] {
  return [...Array(Math.ceil(array.length / size))].map((_, i) => array.slice(i * size, i * size + size));
}
