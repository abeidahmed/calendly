export function nextTick() {
  return new Promise(requestAnimationFrame);
}
