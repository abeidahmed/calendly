import { App, createApp, Component, ComputedOptions, MethodOptions } from 'vue';

export function createVueApp(
  component: Component<unknown, unknown, unknown, ComputedOptions, MethodOptions>,
  selector: string,
  callback?: (app: App<Element>, element: HTMLElement) => void
) {
  let app: App<Element> | undefined;

  // Start mounting the app
  document.addEventListener('turbo:load', () => {
    // Find and mount Vue app on each of the matching HTML elements
    const containers = [...document.querySelectorAll<HTMLElement>(selector)];
    containers.forEach((container) => {
      if (!container) return;

      app = createApp(component);
      callback?.(app, container);
      app.mount(container);
    });
  });

  // Clean up
  document.addEventListener('turbo:before-cache', () => {
    app?.unmount();
    app = undefined;
  });
}
