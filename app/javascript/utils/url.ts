/**
 * @description Construct a URL with a baseURL and query params
 */
export function constructURL(string: string, params: { [key: string]: string | null }) {
  const { url, searchParams } = getURLDetails(string);

  for (const [key, value] of Object.entries(params)) {
    if (value) {
      searchParams.set(key, value);
    } else {
      searchParams.delete(key);
    }
  }

  url.search = searchParams.toString();
  return url;
}

/**
 * @description Get normalized `url` and `searchParams` from a URL or a string
 */
export function getURLDetails(url: string | URL) {
  const baseURL = new URL(url, window.location.href);
  const searchParams = new URLSearchParams(baseURL.search);
  searchParams.sort();

  return { url: baseURL, searchParams };
}
