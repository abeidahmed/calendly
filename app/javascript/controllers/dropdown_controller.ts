import type { Placement } from '@floating-ui/dom';
import { Controller } from '@hotwired/stimulus';
import useOutsideClick from '../stimulus_hooks/use_outside_click';
import usePopupPosition, { UsePopupPositionType } from '../stimulus_hooks/use_popup_position';
import { move, MoveDirection } from '../utils/array';
import { brandedId } from '../utils/string';
import { nextTick } from '../utils/timing';

export default class extends Controller {
  static targets = ['btn', 'menu', 'item'];
  static values = { placement: { type: String, default: 'bottom-start' } };

  poupPosition?: UsePopupPositionType;
  menuId = brandedId();

  connect() {
    useOutsideClick(this, { boundaries: [this.btnTarget, this.menuTarget], callback: this.close.bind(this) });
    this.poupPosition = usePopupPosition(this, {
      reference: this.btnTarget,
      popup: this.menuTarget,
      placement: this.placementValue,
      distance: 4,
      skidding: 0,
    });
    this.linkMenuWithBtn();
  }

  btnTargetConnected(btn: HTMLButtonElement) {
    btn.setAttribute('aria-haspopup', 'true');
    btn.setAttribute('aria-expanded', 'false');
  }

  menuTargetConnected(menu: HTMLElement) {
    if (!menu.hasAttribute('tabindex')) {
      menu.setAttribute('tabindex', '-1');
    }

    if (!menu.hasAttribute('role')) {
      menu.setAttribute('role', 'menu');
    }
  }

  itemTargetConnected(item: HTMLElement) {
    if (!item.hasAttribute('tabindex')) {
      item.setAttribute('tabindex', '0');
    }

    if (!item.hasAttribute('role')) {
      item.setAttribute('role', 'menuitem');
    }
  }

  disconnect() {
    this.close();
  }

  toggle() {
    if (this.isOpen) {
      this.close();
    } else {
      this.open();
    }
  }

  open() {
    if (this.isOpen) return;

    this.dispatch('show', { bubbles: true });
    this.element.setAttribute('open', '');
    this.menuTarget.hidden = false;
    this.btnTarget.setAttribute('aria-expanded', 'true');
    this.poupPosition?.start();
    this.dispatch('shown', { bubbles: true });
  }

  close() {
    if (!this.isOpen) return;

    this.dispatch('hide', { bubbles: true });
    this.element.removeAttribute('open');
    this.menuTarget.hidden = true;
    this.btnTarget.setAttribute('aria-expanded', 'false');
    this.poupPosition?.stop();
    this.dispatch('hidden', { bubbles: true });
  }

  async updatePosition() {
    await this.poupPosition?.update();
  }

  selectItem(event: Event) {
    const { target } = event;
    if (
      !(target instanceof HTMLElement) ||
      target.hasAttribute('disabled') ||
      target.getAttribute('aria-disabled') === 'true'
    )
      return;

    this.close();
    this.dispatch('change', { detail: { relatedTarget: target }, bubbles: true });
  }

  async onKeydown(event: KeyboardEvent) {
    const { target } = event;
    if (!(target instanceof HTMLElement)) return;

    switch (event.key) {
      case 'Escape':
        if (this.isOpen) {
          this.close();
          await nextTick();
          this.btnTarget.focus();
        }
        break;
      case 'Enter':
      case ' ':
        if (!this.isOpen) {
          event.preventDefault();
          event.stopPropagation();
          this.open();
          await nextTick();
          this.focusableItems[0]?.focus();
        } else if (this.isOpen && isMenuItem(target)) {
          event.preventDefault();
          event.stopPropagation();
          target.click();
          await nextTick();
          this.btnTarget.focus();
        }
        break;
      case 'ArrowDown':
        {
          const activeItem = document.activeElement;
          if (!(activeItem instanceof HTMLElement)) return;
          event.preventDefault();
          event.stopPropagation();

          if (isMenuItem(activeItem)) {
            this.move(1);
          } else {
            this.focusableItems[0]?.focus();
          }
        }
        break;
      case 'ArrowUp':
        {
          const activeItem = document.activeElement;
          if (!(activeItem instanceof HTMLElement)) return;
          event.preventDefault();
          event.stopPropagation();

          if (isMenuItem(activeItem)) {
            this.move(-1);
          } else {
            this.focusableItems[this.focusableItems.length - 1]?.focus();
          }
        }
        break;
      case 'Home':
        event.preventDefault();
        event.stopPropagation();
        this.focusableItems[0]?.focus();
        break;
      case 'End':
        event.preventDefault();
        event.stopPropagation();
        this.focusableItems[this.focusableItems.length - 1]?.focus();
        break;
    }
  }

  onOutsideFocus(event: FocusEvent) {
    const target = event.composedPath?.()?.[0] || event.target;
    if (!(target instanceof HTMLElement)) return;

    if (!this.btnTarget.contains(target) && !this.menuTarget.contains(target)) {
      this.close();
    }
  }

  get isOpen() {
    return this.element.hasAttribute('open');
  }

  get focusableItems() {
    return this.itemTargets.filter((item) => !item.hasAttribute('disabled') && !item.hidden);
  }

  private move(direction: MoveDirection) {
    const activeItem = document.activeElement;
    if (!(activeItem instanceof HTMLElement)) return;

    const nextActiveItem = move(this.focusableItems, activeItem, direction);
    nextActiveItem?.focus();
  }

  private linkMenuWithBtn() {
    if (!this.menuTarget.id) {
      this.menuTarget.id = this.menuId;
    }
    this.btnTarget.setAttribute('aria-controls', this.menuTarget.id);
  }

  declare readonly btnTarget: HTMLButtonElement;
  declare readonly menuTarget: HTMLElement;
  declare readonly itemTargets: HTMLElement[];
  declare readonly placementValue: Placement;
}

function isMenuItem(el: Element): boolean {
  const role = el.getAttribute('role');
  return role === 'menuitem' || role === 'menuitemcheckbox' || role === 'menuitemradio';
}
