import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  disconnect() {
    this.hide();
  }

  hide() {
    this.element.remove();
  }
}
