import { defineComponent, h, mergeProps } from 'vue';

export const SketchButton = defineComponent({
  name: 'SketchButton',
  props: {
    type: { type: String, default: 'button' },
    href: { type: [String], default: undefined },
    variant: { type: String, default: 'default' },
  },
  setup(props, { slots }) {
    return () => {
      const { variant, type, ...rest } = props;
      const buttonProps = mergeProps(rest, { type });
      const linkProps = mergeProps(rest, {});
      const classes = {
        btn: true,
        'btn--default': variant === 'default',
        'btn--primary': variant === 'primary',
        'btn--danger': variant === 'danger',
      };
      const tagName = props.href ? 'a' : 'button';
      const mergedProps = { ...(tagName === 'button' ? buttonProps : linkProps), class: classes };

      return h(tagName, mergedProps, {
        default: () => slots.default?.(),
      });
    };
  },
});
