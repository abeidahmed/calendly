module Mutations
  class CreateEvent < BaseMutation
    argument :organization_id, ID
    argument :title, String

    field :id, ID

    def resolve(organization_id:, title:)
      {id: 1}
    end
  end
end
