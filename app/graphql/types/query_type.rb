# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    field :event, EventType do
      argument :id, ID
    end

    def event(id:)
      Event.find(id)
    end

    field :events, [EventType] do
      argument :start_datetime, GraphQL::Types::ISO8601DateTime
      argument :end_datetime, GraphQL::Types::ISO8601DateTime
    end

    def events(start_datetime:, end_datetime:)
      Event.occurrences_between(start_datetime, end_datetime)
    end
  end
end
