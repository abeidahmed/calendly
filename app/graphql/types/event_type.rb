# frozen_string_literal: true

module Types
  class EventType < Types::BaseObject
    field :id, ID, null: false
    field :uid, String, null: false
    field :organization_id, Integer, null: false
    field :title, String, null: false
    field :start_datetime, GraphQL::Types::ISO8601DateTime, null: false
    field :end_datetime, GraphQL::Types::ISO8601DateTime, null: false
    field :span_datetime, GraphQL::Types::ISO8601DateTime
    field :time_zone, String, null: false
    field :all_day, Boolean, null: false
    field :creator_id, Integer, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :recurring_rule, String, null: false
  end
end
