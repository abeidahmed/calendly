# frozen_string_literal: true

class OrganizationsController < ApplicationController
  def index
    @organizations = Organization.includes(:members).where("members.user_id": current_user.id)
    render :index, layout: "slate"
  end

  def new
    @organization = Organization.new
    render :new, layout: "slate"
  end

  def create
    organization = current_user.owned_organizations.build(organization_params)
    organization.members.build(user: current_user)

    if organization.save
      redirect_to dashboards_path(script_name: "/#{organization.id}")
    else
      render_json_errors(organization.errors)
    end
  end

  private

  def organization_params
    params.require(:organization).permit(:name)
  end
end
