# frozen_string_literal: true

module App
  class EventsController < ApplicationController
    def create
      event = current_member.events.build(event_params)

      if event.save
        head :created
      else
        render_json_errors(event.errors)
      end
    end

    private

    def event_params
      params.require(:event).permit(:title, :start_datetime, :end_datetime, :time_zone, :all_day, :recurring_rule)
    end
  end
end
