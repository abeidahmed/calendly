# frozen_string_literal: true

module App::Schedule
  class MonthsController < ApplicationController
    def index
      @start_date = params.fetch(:start_date, Date.current).to_date
    end
  end
end
