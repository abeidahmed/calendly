# frozen_string_literal: true

module App
  class ApplicationController < ::ApplicationController
    include SetCurrentOrganization

    before_action :authenticate_member!

    def current_member
      return if !current_organization || !user_signed_in?
      @current_member ||= current_organization.members.find_by(user_id: current_user.id)
    end
    helper_method :current_member

    private

    def authenticate_member!
      unless current_member
        redirect_to new_user_session_path, status: :unauthorized
      end
    end
  end
end
