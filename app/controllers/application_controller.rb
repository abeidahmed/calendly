# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  alias_method :current_organization, :current_tenant
  helper_method :current_organization

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name])
  end

  def render_json_errors(errors, status: :unprocessable_entity)
    render json: ErrorSerializer.serialize(errors), status: status
  end
end
