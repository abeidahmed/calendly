# frozen_string_literal: true

class BannerComponent < ApplicationComponent
  renders_one :action, types: {
    button: ButtonComponent,
    content: lambda { |**options|
      options[:tag] ||= :div
      BaseComponent.new(**options)
    }
  }

  DEFAULT_VARIANT = :notice
  VARIANT_MAPPINGS = {
    DEFAULT_VARIANT => "bg-blue-50 border-blue-200",
    :success => "bg-green-50 border-green-200",
    :warning => "bg-yellow-50 border-yellow-300",
    :alert => "bg-red-50 border-red-200"
  }.freeze

  def initialize(variant: DEFAULT_VARIANT, full: false, dismissible: true, **options)
    @variant = variant
    @icon = find_icon(options[:icon])
    @dismissible = dismissible
    @options = options
    @options[:tag] ||= :div
    @options[:role] ||= :alert
    @options[:class] = class_names(
      options[:class],
      VARIANT_MAPPINGS[fetch_or_fallback(VARIANT_MAPPINGS.keys, variant, DEFAULT_VARIANT)],
      "py-1 px-4 flex items-start",
      "border-y -mt-px": full,
      "rounded border": !full
    )

    @options[:data] = merge_attributes(
      options[:data],
      controller: dismissible ? "banner" : nil
    )
  end

  private

  def find_icon(name = nil)
    return name if name

    case @variant
    when :success
      "check-circle"
    when :warning
      "exclamation-triangle"
    when :alert
      "x-circle"
    else
      "information-circle"
    end
  end

  def icon_classes
    case @variant
    when :success
      "text-green-700"
    when :warning
      "text-yellow-700"
    when :alert
      "text-red-700"
    else
      "text-blue-700"
    end
  end

  def icon_btn_variant
    case @variant
    when :success
      :success
    when :warning
      :warning
    when :alert
      :danger
    else
      :primary
    end
  end
end
