# frozen_string_literal: true

class ApplicationComponent < ViewComponent::Base
  include Helpers::FetchOrFallbackHelper
  include Helpers::MergeAttributesHelper
end
