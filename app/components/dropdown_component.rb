# frozen_string_literal: true

class DropdownComponent < ApplicationComponent
  renders_one :button, lambda { |component: ButtonComponent, **options|
    options["data-dropdown-target"] = "btn"
    options["data-test-id"] = "dropdown-btn"
    options[:data] = merge_attributes(
      options[:data],
      action: "click->dropdown#toggle"
    )
    component.new(**options)
  }

  renders_one :menu, Dropdown::MenuComponent

  DEFAULT_PLACEMENT = :bottom_start
  PLACEMENT_MAPPINGS = %I[top top_start top_end bottom #{DEFAULT_PLACEMENT} bottom_end right right_start right_end left left_start left_end].freeze

  def initialize(placement: DEFAULT_PLACEMENT, **options)
    @options = options
    @options[:tag] ||= :div
    @options[:class] = class_names(options[:class], "relative")
    @options["data-dropdown-placement-value"] = fetch_or_fallback(PLACEMENT_MAPPINGS, placement, DEFAULT_PLACEMENT).to_s.dasherize
    @options["data-test-id"] = "dropdown"

    @options[:data] = merge_attributes(
      options[:data],
      controller: "dropdown",
      action: "keydown->dropdown#onKeydown focusin@window->dropdown#onOutsideFocus"
    )
  end

  def render?
    button.present? && menu.present?
  end
end
