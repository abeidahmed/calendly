# frozen_string_literal: true

class AvatarComponent < ApplicationComponent
  def initialize(src:, alt:, size: 32, **options)
    @options = options
    @options[:tag] = :img
    @options[:src] = src
    @options[:alt] = alt
    @options[:height] = size
    @options[:width] = size
    @options[:class] = class_names(
      options[:class],
      "inline-block rounded-full bg-gray-100 overflow-hidden flex-shrink-0 leading-none"
    )
  end

  def call
    render(BaseComponent.new(**@options))
  end
end
