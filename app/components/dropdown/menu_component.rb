# frozen_string_literal: true

module Dropdown
  class MenuComponent < ApplicationComponent
    renders_many :items, "Item"

    def initialize(**options)
      @options = options
      @options[:tag] ||= :div
      @options[:class] = class_names(
        options[:class],
        "z-10 py-1 w-56 rounded-md bg-white border shadow-md ring-1 ring-black ring-opacity-5 focus:outline-none"
      )

      @options["data-dropdown-target"] = "menu"
      @options["data-test-id"] = "dropdown-menu"
      @options[:hidden] = true
    end

    INTERACTIVE_ITEM_TAGS = %i[a button input]

    class Item < ApplicationComponent
      def initialize(tag: :a, disabled: false, divider: false, **options)
        @options = options
        @options[:tag] ||= tag
        @options[:tag] = :hr if divider
        @options[:type] ||= :button if tag == :button
        @options[:role] = :separator if divider
        @options[:disabled] = "true" if disabled
        @options["aria-disabled"] = "true" if disabled
        @options[:tabindex] = (disabled || divider) ? "-1" : "0"

        @options[:class] = class_names(
          options[:class],
          "text-gray-700 block w-full text-left px-4 py-2 text-sm hover:bg-gray-100 active:bg-gray-200 focus:outline-none focus-visible:ring-inset focus-visible:ring-2 focus-visible:ring-blue-500": INTERACTIVE_ITEM_TAGS.include?(@options[:tag]),
          "border-t my-2 block": divider
        )

        @options["data-dropdown-target"] = "item" if INTERACTIVE_ITEM_TAGS.include?(@options[:tag])
        @options["data-test-id"] = divider ? "dropdown-divider" : "dropdown-item"

        @options[:data] = merge_attributes(
          options[:data],
          action: divider ? nil : "click->dropdown#selectItem"
        )
      end

      def call
        render(BaseComponent.new(**@options)) { content }
      end
    end
  end
end
