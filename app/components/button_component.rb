# frozen_string_literal: true

class ButtonComponent < ApplicationComponent
  DEFAULT_VARIANT = :default
  VARIANT_MAPPINGS = {
    DEFAULT_VARIANT => {
      default: "text-gray-700 bg-white border-gray-300 hover:bg-gray-100 active:bg-gray-200 focus-visible:ring-blue-500",
      disabled: "text-gray-500 border-gray-300 bg-gray-100"
    },
    :primary => {
      default: "text-white font-semibold bg-blue-500 border-blue-500 hover:bg-blue-600 active:bg-blue-500 focus-visible:ring-blue-500",
      disabled: "text-white font-semibold bg-blue-300 border-blue-300 hover:bg-blue-300 active:bg-blue-300"
    },
    :danger => {
      default: "text-white font-semibold bg-red-500 border-red-500 hover:bg-red-600 active:bg-red-500 focus-visible:ring-red-500",
      disabled: "text-white font-semibold bg-red-300 border-red-300 hover:bg-red-300 active:bg-red-300"
    }
  }.freeze

  def initialize(variant: DEFAULT_VARIANT, disabled: false, **options)
    @options = options
    @options[:tag] ||= :button
    @options[:type] ||= :button if @options[:tag] == :button
    @options[:disabled] = "true" if disabled
    @options["aria-disabled"] = "true" if disabled
    @options[:tabindex] = "-1" if disabled

    @options[:class] = class_names(
      options[:class],
      "inline-flex items-center justify-center border py-[5px] px-3 text-sm rounded appearance-none shadow-sm focus:outline-none focus-visible:ring-offset-2 focus-visible:ring-2",
      fetch_or_fallback(VARIANT_MAPPINGS, variant, DEFAULT_VARIANT, disabled ? :disabled : :default),
      "cursor-not-allowed": disabled,
      "cursor-pointer": !disabled
    )
  end

  def call
    render(BaseComponent.new(**@options)) { content }
  end
end
