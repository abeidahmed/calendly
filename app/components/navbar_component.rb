# frozen_string_literal: true

class NavbarComponent < ApplicationComponent
  def initialize(current_user:)
    @current_user = current_user
  end
end
