# frozen_string_literal: true

module Helpers::FetchOrFallbackHelper
  def fetch_or_fallback(variant_keys, given_value, default_value, key = nil)
    if variant_keys.is_a?(Hash)
      fallback_key = variant_keys.has_key?(given_value) ? given_value : default_value
      variant_keys.dig(fallback_key, key || :default)
    elsif given_value && variant_keys.include?(given_value)
      given_value
    else
      default_value
    end
  end
end
