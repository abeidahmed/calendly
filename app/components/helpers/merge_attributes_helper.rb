# frozen_string_literal: true

module Helpers::MergeAttributesHelper
  def merge_attributes(*args)
    args = args.map { |el| el.presence || {} }

    args.first.deep_merge(args.second) do |_key, val, other_val|
      val + " #{other_val}"
    end
  end
end
