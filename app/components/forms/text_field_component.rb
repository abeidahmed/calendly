# frozen_string_literal: true

module Forms
  class TextFieldComponent < ApplicationComponent
    renders_one :prefix_icon, lambda { |icon:, size: 20, **options|
      options[:size] = size
      options[:class] = class_names(options[:class], "text-gray-500")
      heroicon(icon, **options)
    }

    def initialize(object_name, method_name, help_text: nil, **options)
      @object_name = object_name
      @method_name = method_name
      @help_text = help_text
      @help_text_id = SecureRandom.urlsafe_base64
      @options = options
      @options["aria-describedby"] = @help_text_id if @help_text
      @options[:type] ||= "text"
      @options[:class] = class_names(
        options[:class],
        "form-control block w-full rounded text-sm py-[5px] border-gray-400 focus:ring-blue-500 focus:border-blue-500"
      )
    end
  end
end
