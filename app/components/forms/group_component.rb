# frozen_string_literal: true

class Forms::GroupComponent < ApplicationComponent
  def initialize(**options)
    @options = options
    @options[:tag] ||= :div
    @options[:class] = class_names(
      options[:class],
      "form-group mt-4 first-of-type:mt-0"
    )
  end

  def call
    render(BaseComponent.new(**@options)) { content }
  end

  def render?
    content.present?
  end
end
