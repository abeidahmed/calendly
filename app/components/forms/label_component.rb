# frozen_string_literal: true

module Forms
  class LabelComponent < ApplicationComponent
    def initialize(object_name, method_name, text = nil, **options)
      @object_name = object_name
      @method_name = method_name
      @text = text
      @options = options
      @options[:class] = class_names(options[:class], "inline-block font-semibold")
    end

    def call
      if content.present?
        @view_context.label(@object_name, @method_name, @text, **@options) { content }
      else
        @view_context.label(@object_name, @method_name, @text, **@options)
      end
    end
  end
end
