# frozen_string_literal: true

module Forms
  class HelpTextComponent < ApplicationComponent
    def initialize(**options)
      @options = options
      @options[:tag] ||= :span
      @options["data-test-id"] = "form-help-text"
      @options[:class] = class_names(
        options[:class],
        "inline-block mt-1 text-gray-600"
      )
    end

    def call
      render(BaseComponent.new(**@options)) { content }
    end

    def render?
      content.present?
    end
  end
end
