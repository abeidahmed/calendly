# frozen_string_literal: true

module Schedule
  class MonthGridComponent < ApplicationComponent
    def initialize(start_date:)
      @start_date = start_date
    end

    private

    # First date of the previous month/current month should have the month name
    # Current date (today) should have the month name
    # First date of the next month should have the month name
    def format_day(day)
      if day.today? || (day.beginning_of_week == day && !current_month?(day)) || day.beginning_of_month == day
        day.strftime("%d %b")
      else
        day.strftime("%d")
      end
    end

    def date_range
      (@start_date.beginning_of_month.beginning_of_week..@start_date.end_of_month.end_of_week).to_a
    end

    def day_classes(day)
      class_names(
        "text-blue-600 font-semibold": day.today?,
        "text-gray-600": !current_month?(day)
      )
    end

    def current_month?(day)
      day.month == @start_date.month
    end
  end
end
