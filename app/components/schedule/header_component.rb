# frozen_string_literal: true

module Schedule
  class HeaderComponent < ApplicationComponent
    renders_one :view_heading, lambda { |**options|
      options[:tag] ||= :div
      options[:class] = class_names(options[:class], "font-semibold")

      BaseComponent.new(**options)
    }

    def initialize(start_date:, end_date:)
      @start_date = start_date
      @end_date = end_date
    end

    private

    def previous_month_url
      url_for(start_date: (@start_date - 1.day).iso8601)
    end

    def next_month_url
      url_for(start_date: (@end_date + 1.day).iso8601)
    end
  end
end
