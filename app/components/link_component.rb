# frozen_string_literal: true

class LinkComponent < ApplicationComponent
  def initialize(**options)
    @options = options
    @options[:tag] ||= :a
    @options[:href] ||= "#" if @options[:tag] == :a
    @options[:type] ||= "button" if @options[:tag] == :button
    @options[:class] = class_names(
      options[:class],
      "text-blue-600 hover:underline"
    )
  end

  def call
    render(BaseComponent.new(**@options)) { content }
  end
end
