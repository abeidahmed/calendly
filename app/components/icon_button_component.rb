# frozen_string_literal: true

class IconButtonComponent < ApplicationComponent
  DEFAULT_SIZE = 20

  DEFAULT_VARIANT = :default
  VARIANT_MAPPINGS = {
    DEFAULT_VARIANT => {
      default: "text-gray-700 hover:text-gray-900 hover:bg-gray-100 active:bg-gray-200 focus-visible:ring-blue-500",
      disabled: "text-gray-400"
    },
    :primary => {
      default: "text-blue-700 hover:text-blue-900 hover:bg-blue-50 active:bg-blue-100 focus-visible:ring-blue-500",
      disabled: "text-blue-400"
    },
    :success => {
      default: "text-green-700 hover:text-green-900 hover:bg-green-50 active:bg-green-100 focus-visible:ring-green-500",
      disabled: "text-green-400"
    },
    :warning => {
      default: "text-yellow-700 hover:text-yellow-900 hover:bg-yellow-50 active:bg-yellow-100 focus-visible:ring-yellow-500",
      disabled: "text-yellow-500"
    },
    :danger => {
      default: "text-red-700 hover:text-red-900 hover:bg-red-50 active:bg-red-100 focus-visible:ring-red-500",
      disabled: "text-red-400"
    }
  }.freeze

  DEFAULT_ICON_VARIANT = :outline
  ICON_VARIANT_MAPPINGS = [DEFAULT_ICON_VARIANT, :solid, :mini].freeze

  def initialize(
    icon:, title:, size: DEFAULT_SIZE, variant: DEFAULT_VARIANT, icon_variant: DEFAULT_ICON_VARIANT, disabled: false,
    **options
  )
    @icon = icon
    @size = size
    @variant = icon_variant
    @options = options
    @options[:title] = title
    @options[:tag] ||= :button
    @options[:type] ||= :button if @options[:tag] == :button
    @options[:disabled] = "true" if disabled
    @options["aria-disabled"] = "true" if disabled
    @options[:tabindex] = "-1" if disabled
    @options[:class] = class_names(
      options[:class],
      fetch_or_fallback(VARIANT_MAPPINGS, variant, DEFAULT_VARIANT, disabled ? :disabled : :default),
      "py-[5px] px-[5px] inline-flex items-center justify-center rounded border border-transparent appearance-none focus:outline-none focus-visible:ring-2"
    )
  end

  def call
    render(BaseComponent.new(**@options)) do
      heroicon(@icon, size: @size, variant: fetch_or_fallback(ICON_VARIANT_MAPPINGS, @variant, DEFAULT_ICON_VARIANT))
    end
  end
end
