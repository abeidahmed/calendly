require_relative "boot"

require "rails/all"
require_relative "../lib/middlewares/organization_url_prefixer"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv::Railtie.load unless Rails.env.production?

module Calendly
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Avoid generating these files on scaffold
    config.generators.stylesheets = false
    config.generators.helper = false
    config.generators.helper_specs = false

    config.generators.test_framework :test_unit, fixture: false

    config.to_prepare do
      Devise::SessionsController.layout "slate"
      Devise::RegistrationsController.layout "slate"
      Devise::PasswordsController.layout "slate"
    end

    # Sunday should be the start of the week. Rails defaults to `monday`
    config.beginning_of_week = :sunday

    # Middlewares
    config.middleware.use OrganizationUrlPrefixer

    # ViewComponent
    config.view_component.default_preview_layout = "previews/application"
    config.view_component.show_previews = true

    # Lookbook
    config.lookbook.project_name = "Calendly"
    config.lookbook.debug_menu = true
    config.lookbook.preview_params_options_eval = true
  end
end
