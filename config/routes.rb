require_relative "../lib/constraints/marketing_constraint"
require_relative "../lib/constraints/organization_constraint"

Rails.application.routes.draw do
  root "static_pages#home"
  post "/graphql", to: "graphql#execute"

  # Do not allow routes with pattern "/1/*"
  constraints MarketingConstraint do
    devise_for :users, controllers: {
      registrations: "registrations",
      sessions: "sessions"
    }

    resources :organizations, only: %i[index new create], path: "orgs"
  end

  # Only allow routes that match the pattern "/1/*", where "1" is the organization id
  constraints OrganizationConstraint do
    scope module: :app do
      resources :dashboards, only: :index, path: "dashboard"
      resources :events, only: :create

      namespace :schedule do
        resources :months, only: :index, path: "month"
      end
    end
  end

  if Rails.env.development?
    mount Lookbook::Engine, at: "/lookbook"
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
end
